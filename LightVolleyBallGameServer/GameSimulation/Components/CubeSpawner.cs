﻿using System.Collections.Generic;
using LightEngineGameSimulation.Components;
using LightEngineGameSimulation.PhysicsEngine.Common;
using LightEngineGameSimulation.PhysicsEngine.Dynamics;
using LightVolleyBallBridge.Enum;
using MathHelper = LightEngineGameSimulation.Common.MathHelper;

namespace LightVolleyBallGameServer.GameSimulation.Components
{
    public class CubeSpawner : Component
    {
        private readonly Game _game;
        private readonly List<GameObject> _cubes = new List<GameObject>();

        public CubeSpawner(Game game)
        {
            _game = game;
        }

        public override void OnAssigned()
        {
            this.gameObject.GameLoop.Invoke(() =>
                {
                    var cube = new GameObject("Cube");
                    cube.AddComponent(new Rigidbody2D(1f, 1f, 1f,
                        new Vector2(MathHelper.NextFloat(-5, 5),
                            MathHelper.NextFloat(-5, 5)), BodyType.Dynamic));
                    cube.AddComponent(new TransformSync(_game));
                    cube.AddComponent(new StateSync(NetworkObjectType.Cube, _game));
                    cube.AddComponent(new Movement());
                    this.gameObject.GameLoop.RegisterGameObject(cube);
                    _cubes.Add(cube);
                }, 10)
                .SetInvokeCount(25)
                .SetEnsureInterval(true);
        }

        public void MakeEplosion(Vector2 pos)
        {
            foreach (var c in _cubes)
            {
                if (Vector2.Distance(c.Pos, pos) > 10) continue;
                var force = c.Pos - pos;
                force.Normalize();
                force *= 1000;
                c.GetComponent<Rigidbody2D>().body.ApplyForce(force, pos);
            }
        }
    }
}