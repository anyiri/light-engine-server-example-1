﻿using System.Linq;
using LightEngineBridge.LiteNetLib;
using LightEngineBridge.Util;
using LightEngineGameSimulation.Common;
using LightEngineGameSimulation.Components;
using LightGameServer.Abstract;
using LightVolleyBallBridge.Model;

namespace LightVolleyBallGameServer.GameSimulation.Components
{
    public class TransformSync : Component
    {
        private readonly SimulatedGame _game;

        public TransformSync(SimulatedGame game)
        {
            _game = game;
        }

        public override void OnAssigned()
        {
        }

        public override void Update()
        {
            var pos = gameObject.Transform.Position;
            var gameContext = _game.GetContext<GameContext>();
            _game.Server.SenderFactory.CreateSender<TransformSyncModel>(gameContext.Players.ThreadSafeCopy().Select(p => p.NetPeer).ToList())
                .Send(DeliveryMethod.Sequenced,
                    new TransformSyncModel
                    {
                        GameObjectId = gameObject.Id,
                        PosX = pos.X,
                        PosY = pos.Y,
                        Timestamp = _game.GameLoop.Time,
                        Rotation = gameObject.Transform.Rotation.ToDegrees()
                    });
        }
    }
}
