﻿using System.Linq;
using LightEngineBridge.LiteNetLib;
using LightEngineGameSimulation.Common;
using LightEngineGameSimulation.Components;
using LightEngineGameSimulation.PhysicsEngine.Common;
using LightGameServer.Abstract;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;

namespace LightVolleyBallGameServer.GameSimulation.Components
{
    public class StateSync : Component
    {
        private readonly NetworkObjectType _networkObjectType;
        private readonly SimulatedGame _game;

        public StateSync(NetworkObjectType networkObjectType, SimulatedGame game)
        {
            _networkObjectType = networkObjectType;
            _game = game;
        }

        public override void OnAssigned()
        {
            SendSpawn();
        }

        public override void Destroy()
        {
            SendDestroy();
        }

        public void SendSpawn(NetPeer peer)
        {
            _game.Server.SenderFactory.CreateSender<GameObjectSpawnModel>(peer)
                .Send(DeliveryMethod.ReliableOrdered,
                    CreateSpawnModel());
        }

        public void SendSpawn()
        {
            var gameContext = _game.GetContext<GameContext>();
            _game.Server.SenderFactory
                .CreateSender<GameObjectSpawnModel>(gameContext.Players.ThreadSafeCopy().Select(p => p.NetPeer).ToList())
                .Send(DeliveryMethod.ReliableOrdered,
                    CreateSpawnModel());
        }

        private void SendDestroy()
        {
            var gameContext = _game.GetContext<GameContext>();
            _game.Server.SenderFactory
                .CreateSender<GameObjectDestroyModel>(gameContext.Players.ThreadSafeCopy().Select(p => p.NetPeer).ToList())
                .Send(DeliveryMethod.ReliableOrdered,
                    new GameObjectDestroyModel {GameObjectId = gameObject.Id});
        }

        private GameObjectSpawnModel CreateSpawnModel()
        {
            Vector2 pos = gameObject.GetComponent<Rigidbody2D>().body.Position;
            return new GameObjectSpawnModel
            {
                GameObjectId = gameObject.Id,
                NetworkObjectType = _networkObjectType,
                PosX = pos.X,
                PosY = pos.Y
            };
        }
    }
}