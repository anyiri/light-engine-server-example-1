﻿using LightEngineGameSimulation.Components;

namespace LightVolleyBallGameServer.GameSimulation.Components
{
    public class Movement : Component
    {
        public override void OnAssigned()
        {
            var rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
            var body = rigidbody2D.body;
            body.LinearDamping = 0.5f;
            body.Friction = 0.2f;
            body.Restitution = 0.6f;
            body.AngularDamping = 1f;
        }
    }
}