﻿using System.Collections.Generic;
using LightEngineBridge.LiteNetLib;
using LightVolleyBallGameServer.Client;

namespace LightVolleyBallGameServer.GameSimulation
{
    public class GameContext
    {
        public List<PeerInfo> Players { get; set; } = new List<PeerInfo>();
    }
}