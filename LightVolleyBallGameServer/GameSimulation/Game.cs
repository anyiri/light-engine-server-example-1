﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using LightEngineBridge.LiteNetLib;
using LightEngineBridge.Serialization;
using LightEngineGameSimulation.Common;
using LightEngineGameSimulation.Components;
using LightEngineGameSimulation.Loop;
using LightEngineGameSimulation.PhysicsEngine.Common;
using LightEngineGameSimulation.PhysicsEngine.Dynamics;
using LightGameServer.Abstract;
using LightVolleyBallBridge.Model;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.GameSimulation.Components;

namespace LightVolleyBallGameServer.GameSimulation
{
    public class Game : SimulatedGame
    {
        public override void Start()
        {
            this.GameLoop.PhysicsWorld.Gravity = new Vector2(0, -10f);
            var spawner = new GameObject("Cube Spawner");
            spawner.AddComponent(new CubeSpawner(this));
            GameLoop.RegisterGameObject(spawner);
            CreateWalls();
        }

        private void CreateWalls()
        {
            GameLoop.RegisterGameObject(new GameObject("Wall1"))
                .AddComponent(new Rigidbody2D(1, 20, 1, new Vector2(-15, 0), BodyType.Static));
            GameLoop.RegisterGameObject(new GameObject("Wall2"))
                .AddComponent(new Rigidbody2D(1, 20, 1, new Vector2(15, 0), BodyType.Static));
            GameLoop.RegisterGameObject(new GameObject("Wall3"))
                .AddComponent(new Rigidbody2D(30, 1, 1, new Vector2(0, 10), BodyType.Static));
            GameLoop.RegisterGameObject(new GameObject("Wall4"))
                .AddComponent(new Rigidbody2D(30, 1, 1, new Vector2(0, -10), BodyType.Static));
        }

        public void MakeExplosion(Vector2 position)
        {
            GameLoop.ActiveObjects.First(g => g.Name == "Cube Spawner").GetComponent<CubeSpawner>()
                .MakeEplosion(position);
        }

        public void AddPlayer(PeerInfo peerInfo)
        {
            GetContext<GameContext>().Players.AddThreadSafe(peerInfo);
            ResyncPlayer(peerInfo);
        }
        private void ResyncPlayer(PeerInfo peerInfo)
        {
            SendReliable(peerInfo.NetPeer,
                new GameResyncModel {GameObjectIds = GameLoop.ActiveObjects.Select(g => g.Id).ToArray()});
            GameLoop.ActiveObjects.ForEach(g =>
                g.GetComponent<StateSync>()?.SendSpawn(peerInfo.NetPeer)
            );
        }

        public void SendReliable<TSendModel>(NetPeer peer, TSendModel sendModel)
            where TSendModel : BridgeModel
        {
            SendData(peer, sendModel, DeliveryMethod.ReliableOrdered);
        }

        public void SendSequenced<TSendModel>(NetPeer peer, TSendModel sendModel)
            where TSendModel : BridgeModel
        {
            SendData(peer, sendModel, DeliveryMethod.Sequenced);
        }

        private void SendData<TSendModel>(NetPeer peer, TSendModel sendModel, DeliveryMethod deliveryMethod)
            where TSendModel : BridgeModel
        {
            this.Server.SenderFactory.CreateSender<TSendModel>(peer).Send(deliveryMethod, sendModel);
        }
    }
}