﻿using System;
using LightEngineBridge.Debug;

namespace LightVolleyBallGameServer.Debug
{
    public class Logger : ILightLogger
    {
        public void Info(string message)
        {
            Console.WriteLine(message);
        }

        public void Info(Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        public void Warn(string message)
        {
            Console.WriteLine(message);
        }

        public void Warn(Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        public void Error(string message)
        {
            Console.WriteLine(message);
        }

        public void Error(Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}
