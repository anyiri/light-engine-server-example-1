﻿using LightEngineBridge.Network.Task.Base;
using LightEngineBridge.Serialization;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.Task.Context;

namespace LightVolleyBallGameServer.Task.Extender
{
    public abstract class
        ReceiverTask<TReceiverModel> : ReceiverNetworkTask<TReceiverModel, TaskContext>
        where TReceiverModel : BridgeModel, new()
    {
        protected PeerInfo PlayerPeerInfo => this.TaskContext.Server.ClientManager.GetState<PeerInfo>(this.Peer);
    }
}
