﻿using System.Runtime.CompilerServices;
using LightEngineBridge.Model;
using LightEngineBridge.Network.Task.Base;
using LightEngineBridge.Serialization;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.Task.Context;

namespace LightVolleyBallGameServer.Task.Extender
{
    public abstract class
        SenderTask<TSendModel> : SenderNetworkTask<TSendModel, TaskContext>
        where TSendModel : BridgeModel
    {
        protected PeerInfo PlayerPeerInfo => this.TaskContext.Server.ClientManager.GetState<PeerInfo>(this.Peer);
    }
}
