﻿using LightEngineBridge.Network.Task.Base;
using LightEngineBridge.Serialization;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.Task.Context;

namespace LightVolleyBallGameServer.Task.Extender
{
    public abstract class
        FullTask<TReceiveModel, TSendModel> : FullNetworkTask<TReceiveModel, TSendModel, TaskContext>
        where TReceiveModel : BridgeModel, new()
        where TSendModel : BridgeModel
    {
        protected PeerInfo PlayerPeerInfo => this.TaskContext.Server.ClientManager.GetState<PeerInfo>(this.Peer);
    }
}