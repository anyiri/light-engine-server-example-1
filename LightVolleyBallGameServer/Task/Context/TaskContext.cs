﻿using LightGameServer.Network;

namespace LightVolleyBallGameServer.Task.Context
{
    public class TaskContext
    {
        public TaskContext(LightServer server)
        {
            Server = server;
        }

        public LightServer Server { get; }
    }
}
