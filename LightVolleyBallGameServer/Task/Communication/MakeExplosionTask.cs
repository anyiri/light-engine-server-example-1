﻿using LightEngineBridge.Attribute;
using LightEngineGameSimulation.PhysicsEngine.Common;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using LightVolleyBallGameServer.Task.Extender;

namespace LightVolleyBallGameServer.Task.Communication
{
    [NetworkTask(NetworkCommand.MakeExplosion)]
    public class MakeExplosionTask : ReceiverTask<MakeExplosionModel>
    {
        public override void Execute()
        {
            PlayerPeerInfo.CurrentGame?.MakeExplosion(new Vector2(Data.PosX, Data.PosY));
        }
    }
}