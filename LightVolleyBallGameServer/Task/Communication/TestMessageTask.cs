﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using LightVolleyBallGameServer.Task.Context;

namespace LightVolleyBallGameServer.Task.Communication
{
    [NetworkTask(NetworkCommand.TestMessage, AsyncExecute = true)]
    public class TestMessageTask : FullNetworkTask<StringModel, StringModel, TaskContext>
    {
        public override void Execute()
        {
            var str = $"Server received: {Data.Value}";
            SendBack(new StringModel {Value = str});
        }
    }
}