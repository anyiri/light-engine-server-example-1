﻿using System.Collections.Generic;
using System.Linq;
using LightEngineBridge.Attribute;
using LightEngineBridge.Util;
using LightEngineGameSimulation.Common;
using LightGameServer.Abstract;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.GameSimulation;
using LightVolleyBallGameServer.Task.Extender;

namespace LightVolleyBallGameServer.Task.Communication
{
    [NetworkTask(NetworkCommand.StartGame)]
    public class StartGameTask : SenderTask<StartGameModel>
    {
        public override void Execute()
        {
            var peerInfo = this.PlayerPeerInfo;
            if (peerInfo.CurrentGame != null) return;
            var gameManager = TaskContext.Server.GameManager;
            Game game;
            var simulatedGames = gameManager.GetGames();
            if (simulatedGames.Any())
            {
                game = simulatedGames.First().Cast<Game>();
                game.AddPlayer(peerInfo);
            }
            else
            {
                game = gameManager.SimulateGame(new GameContext {Players = new List<PeerInfo> {peerInfo}}).Cast<Game>();
            }

            peerInfo.CurrentGame = game;
            SendBack(new StartGameModel {Timestamp = game.GameLoop.Time});
        }
    }
}