﻿using LightEngineBridge.LiteNetLib;
using LightGameServer.Abstract;
using LightVolleyBallGameServer.GameSimulation;

namespace LightVolleyBallGameServer.Client
{
    public class PeerInfo
    {
        public NetPeer NetPeer { get; set; }
        public bool IsConnected => this.NetPeer != null && this.NetPeer.ConnectionState == ConnectionState.Connected;
        public Game CurrentGame { get; set; }
    }
}