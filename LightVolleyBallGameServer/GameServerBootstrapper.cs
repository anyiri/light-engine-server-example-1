﻿using System;
using System.Linq;
using LightEngineBridge.Debug;
using LightEngineBridge.LiteNetLib;
using LightEngineGameSimulation.Common;
using LightEngineGameSimulation.Configuration;
using LightGameServer.ConsoleStuff;
using LightGameServer.Network;
using LightVolleyBallGameServer.Client;
using LightVolleyBallGameServer.Debug;
using LightVolleyBallGameServer.GameSimulation;
using LightVolleyBallGameServer.Task.Context;

namespace LightVolleyBallGameServer
{
    public class GameServerBootstrapper
    {
        private LightServer _lightServer;

        public void StartServer()
        {
            var config = CreateServerConfiguration();
            _lightServer = new LightServer(config);
            _lightServer.PeerConnectedEvent += OnPlayerConnected;
            _lightServer.PeerDisconnectedEvent += OnPlayerDisconnected;
            _lightServer.Start();
        }

        private ConsoleMenu CreateConsoleMenu()
        {
            ConsoleMenu menu = new ConsoleMenu("Light Volleyball Game Server");
            menu.Add("Print online player count",
                    () => { Console.WriteLine($"Online players: {_lightServer.ClientManager.GetPlayerCount()}"); })
                .Add("Print currently simulated game count",
                    () => { Console.WriteLine($"Simulated games: {_lightServer.GameManager.GetGames().Count}"); })
                .Add("Print all game simulation FPS",
                    () => { _lightServer.GameManager.GetGames().ForEach(g => Console.WriteLine(g.GameLoop.Fps)); })
                .Add("Clear console", menu.Refresh);

            return menu;
        }

        private ServerConfiguration CreateServerConfiguration()
        {
            LightDebug.Logger = new Logger();
            LightGameSimulationSettings.targetFrameRate = 30;
            LightGameSimulationSettings.fixedTimeStep = 0.033f;
            var config = new ServerConfiguration();
            config.SetConnectionKey("alma");
            config.SetPort(60001);
            config.SetConsoleMenu(CreateConsoleMenu());
            config.SetTaskContextFactory(s => new TaskContext(s));
            config.SetClientData<PeerInfo>();
            config.SetSimulatedGame<Game>();
            return config;
        }

        private void OnPlayerConnected(NetPeer peer)
        {
            _lightServer.ClientManager.SetState(peer, new PeerInfo {NetPeer = peer});
        }

        private void OnPlayerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            var peerInfo = _lightServer.ClientManager.GetState<PeerInfo>(peer);
            var currentGame = peerInfo.CurrentGame;
            if (currentGame != null)
            {
                currentGame.GetContext<GameContext>().Players.RemoveThreadSafe(peerInfo);
                if (!currentGame.GetContext<GameContext>().Players.ThreadSafeCopy().Any())
                {
                    _lightServer.GameManager.StopSimulation(currentGame);
                }
            }
        }
    }
}