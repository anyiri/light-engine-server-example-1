﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using LightEngineBridge.Attribute;
using LightEngineBridge.LiteNetLib.Utils;
using LightEngineBridge.Serialization;
using LightEngineBridge.Util;
using LightEngineGameSimulation.PhysicsEngine.Dynamics;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using LightVolleyBallGameServer.GameSimulation;

namespace LightVolleyBallGameServer
{
    class LightVolleyBallServerEntry
    {
        static void Main(string[] args)
        {
            //TransformSyncModel originalModel = new TransformSyncModel
            //{
            //    GameObjectId = 1,
            //    PosX = 32,
            //    PosY = 43,
            //    Rotation = 213f,
            //    Timestamp = 123f
            //};

            //    Console.WriteLine("My Serializer Start");
            //    LightSerializer.TestSerializationSpeed(originalModel, 1000000);
            //    Console.WriteLine("My Serializer End");

            //Console.ReadKey(true);

            GameServerBootstrapper bootstrapper = new GameServerBootstrapper();
            bootstrapper.StartServer();
        }
        
    }
}