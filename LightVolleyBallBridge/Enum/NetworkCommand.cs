﻿namespace LightVolleyBallBridge.Enum
{
    public enum NetworkCommand : byte
    {
        TestMessage,
        PositionSync,
        StartGame,
        GameObjectSpawn,
        GameObjectDestroy,
        TransformSync,
        GameResync,
        MakeExplosion
    }
}