﻿using LightEngineBridge.LiteNetLib.Utils;
using LightEngineBridge.Serialization;

namespace LightVolleyBallBridge.Model
{
    public class StringModel : BridgeModel
    {
        public string Value { get; set; }

        //public override NetDataWriter Serialize(byte commandByte)
        //{
        //    NetDataWriter writer = new NetDataWriter();
        //    writer.Put(commandByte);
        //    writer.Put(Value);
        //    return writer;
        //}

        //public override BridgeModel Deserialize(NetDataReader reader)
        //{
        //    return new StringModel
        //    {
        //        Value = reader.GetString()
        //    };
        //}
    }
}
