﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Serialization;
using LightVolleyBallBridge.Enum;

namespace LightVolleyBallBridge.Model
{
    [TaskModel(NetworkCommand.GameObjectDestroy)]
    public class GameObjectDestroyModel : BridgeModel
    {
        public ushort GameObjectId { get; set; }
    }
}
