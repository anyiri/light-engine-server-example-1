﻿using LightEngineBridge.Attribute;
using LightVolleyBallBridge.Enum;

namespace LightVolleyBallBridge.Model
{
    [TaskModel(NetworkCommand.PositionSync)]
    public class PositionSyncModel : TimestampMarked
    {
        public ushort GameObjectId { get; set; }
        public short PosX { get; set; }
        public short PosY { get; set; }
    }
}
