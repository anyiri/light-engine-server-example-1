﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Serialization;
using LightVolleyBallBridge.Enum;

namespace LightVolleyBallBridge.Model
{
    [TaskModel(NetworkCommand.GameResync)]
    public class GameResyncModel : BridgeModel
    {
        public ushort[] GameObjectIds { get; set; }
    }
}