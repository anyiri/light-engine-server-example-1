﻿using LightEngineBridge.Serialization;

namespace LightVolleyBallBridge.Model
{
    public class MakeExplosionModel : BridgeModel
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
    }
}
