﻿using LightEngineBridge.Attribute;
using LightEngineBridge.LiteNetLib.Utils;
using LightEngineBridge.Serialization;
using LightVolleyBallBridge.Enum;

namespace LightVolleyBallBridge.Model
{
    [TaskModel(NetworkCommand.TransformSync)]
    public class TransformSyncModel : TimestampMarked
    {
        public ushort GameObjectId { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float Rotation { get; set; }

        public override NetDataWriter Serialize(byte commandByte)
        {
            NetDataWriter writer = new NetDataWriter();
            writer.Put(commandByte);
            writer.Put(GameObjectId);
            writer.Put(PosX);
            writer.Put(PosY);
            writer.Put(Rotation);
            writer.Put(Timestamp);
            return writer;
        }

        public override BridgeModel Deserialize(NetDataReader reader)
        {
            return new TransformSyncModel
            {
                GameObjectId = reader.GetUShort(),
                PosX = reader.GetFloat(),
                PosY = reader.GetFloat(),
                Rotation = reader.GetFloat(),
                Timestamp = reader.GetFloat()
            };
        }
    }
}
