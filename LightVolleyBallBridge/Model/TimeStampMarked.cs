﻿using LightEngineBridge.Serialization;

namespace LightVolleyBallBridge.Model
{
    public class TimestampMarked : BridgeModel
    {
        public float Timestamp { get; set; }
    }
}
