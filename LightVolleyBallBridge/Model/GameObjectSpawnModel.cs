﻿using LightEngineBridge.Attribute;
using LightEngineBridge.LiteNetLib.Utils;
using LightEngineBridge.Serialization;
using LightVolleyBallBridge.Enum;

namespace LightVolleyBallBridge.Model
{
    [TaskModel(NetworkCommand.GameObjectSpawn)]
    public class GameObjectSpawnModel : BridgeModel
    {
        public ushort GameObjectId { get; set; }
        public NetworkObjectType NetworkObjectType { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }

        //public override NetDataWriter Serialize(byte commandByte)
        //{
        //    NetDataWriter writer = new NetDataWriter();
        //    writer.Put(commandByte);
        //    writer.Put(GameObjectId);
        //    writer.Put((byte)NetworkObjectType);
        //    writer.Put(PosX);
        //    writer.Put(PosY);
        //    return writer;
        //}

        //public override BridgeModel Deserialize(NetDataReader reader)
        //{
        //    return new GameObjectSpawnModel
        //    {
        //        GameObjectId = reader.GetUShort(),
        //        NetworkObjectType = (NetworkObjectType)reader.GetByte(),
        //        PosX = reader.GetFloat(),
        //        PosY = reader.GetFloat()
        //    };
        //}
    }
}